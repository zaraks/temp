import os
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

# import MNIST data
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)

def show_grid_3x3(images):
    """
    Display a 3x3 grid of 9 randomly sampled numpy array images.
    : images: A batch of image data. Numpy array with shape (batch_size, 28, 28, 1)
    """
    plt.rcParams['figure.figsize'] = 6, 6
    fig, axes = plt.subplots(nrows=3, ncols=3, sharex=True, sharey=True)
    rand_idx = np.random.choice(images.shape[0], 9, replace=False) # get 5 random indices
    images = images[rand_idx]
    for i in range(3):
        for j in range(3):
            axes[i, j].imshow(images[i + 3*j].reshape((28, 28)), cmap='gray')
            plt.tight_layout()

show_grid_3x3(mnist.train.images)

# For logging
from datetime import datetime

def logdir_timestamp(root_logdir="tf_logs"):
    """Return a string with a timestamp to use as the log directory for TensorBoard."""
    now = datetime.utcnow().strftime("%Y%m%d_%H%M%S")
    return os.path.join(root_logdir, "run-{}/".format(now))

logdir = logdir_timestamp()

def neural_net_image_input(image_shape):
    """Constructs a tensor for a batch of image input
    image_shape: Shape of the images as a list-like object
    return: Tensor for image input
    """
    shape = None, *image_shape
    return tf.placeholder(tf.float32,
                          shape=shape,
                          name="X")

def neural_net_label_input(n_classes):
    """Constructs a tensor for a batch of label input
    n_classes: Number of classes
    return: Tensor for label input
    """
    shape = None, n_classes
    return tf.placeholder(tf.float32,
                          shape=shape,
                          name="y")

def convolution_layer(inp, size_in, size_out, name="convolution"):
    """Creates a convolutional layer with filter of size 5x5, and size_out number of filters.
    Applies stride of [1, 1, 1, 1] with SAME padding, and appies ReLU activation.
    No downsampling within this layer - returns tensor with activation function applied only
    """
    with tf.name_scope(name):
        # Hard code convolutional filter of size 5x5
        W = tf.Variable(tf.truncated_normal([5, 5, size_in, size_out], stddev=0.1), name="W")
        b = tf.Variable(tf.constant(0.1, shape=[size_out]), name="b") 
        conv = tf.nn.conv2d(inp, W, strides=[1, 1, 1, 1], padding='SAME')
        act = tf.nn.relu(conv + b)
        tf.summary.histogram("weights", W)
        tf.summary.histogram("biases", b)
        tf.summary.histogram("activations", act)
        return act

def downsample_layer(act, name="maxpooling"):
    """Creates downsampling layer by applying maxpooling with hardcode kernel size 
    [1, 2, 2, 1] and strides [1, 2, 2, 1] with SAME padding.
    """
    with tf.name_scope(name):
        return tf.nn.max_pool(act, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')

def dense_layer(inp, size_in, size_out, name="dense"):
    """Creates fully connected layer with size [size_in, size_out]. Initialize weights with 
    standard deviation 0.1. Returns tensor without applying any activation function.
    """
    with tf.name_scope(name):
        W = tf.Variable(tf.truncated_normal([size_in, size_out], stddev=0.1), name="W")
        b = tf.Variable(tf.constant(0.1, shape=[size_out]), name="b") 
        act = tf.matmul(inp, W) + b
        tf.summary.histogram("weights", W)
        tf.summary.histogram("biases", b)
        tf.summary.histogram("activations", act)
        return act

def dropout(inp, keep_prob, name="dropout"):
    """Apply dropout with probability defined by placeholder tensor keep_prob."""
    with tf.name_scope(name):
        return tf.nn.dropout(inp, keep_prob)

tf.reset_default_graph()

lr=1e-2 # Learning rate
X = neural_net_image_input([784])
y_ = neural_net_label_input(10)
X_image = tf.reshape(X, [-1, 28, 28, 1]) #  rehaped to [batch_size, rows, cols, channels]
keep_prob = tf.placeholder(tf.float32)

act1 = convolution_layer(X_image, 1, 32, "convolution1")
h_pool1 = downsample_layer(act1, "downsample1")
act2 = convolution_layer(h_pool1, 32, 64, "convolution2")
h_pool2 = downsample_layer(act2, "downsample2")
h_pool2_flat = tf.reshape(h_pool2, [-1, 7*7*64])
h_fc1 = tf.nn.relu(dense_layer(h_pool2_flat, 7*7*64, 1024, "dense1"))
h_fc1_drop = dropout(h_fc1, keep_prob)
y_conv = dense_layer(h_fc1_drop, 1024, 10, "dense2")

with tf.name_scope("xentropy"):
    cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y_conv))

with tf.name_scope("train"):
    optimizer = tf.train.AdamOptimizer(learning_rate=lr)
    training_op = optimizer.minimize(cross_entropy)

with tf.name_scope("accuracy"):
    correct_prediction = tf.equal(tf.argmax(y_conv, 1), tf.argmax(y_, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, dtype=tf.float32))
    tf.summary.scalar('accuracy', accuracy)

write_op = tf.summary.merge_all()
writer_train = tf.summary.FileWriter(logdir + 'train', tf.get_default_graph())
writer_val = tf.summary.FileWriter(logdir + 'val', tf.get_default_graph())

if __name__ == "__main__":
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        for i in range(1001):
            batch_X, batch_y = mnist.train.next_batch(100)
            val_batch_X, val_batch_y = mnist.validation.next_batch(100)
            if i % 5 == 0:
                summary_str = sess.run(write_op, feed_dict={X: batch_X, y_: batch_y, keep_prob: 1.0})
                writer_train.add_summary(summary_str, i)
                writer_train.flush()
                summary_str = sess.run(write_op, feed_dict={X: val_batch_X, y_: val_batch_y, keep_prob: 1.0})
                writer_val.add_summary(summary_str, i)
                writer_val.flush()
            training_op.run(feed_dict={X: batch_X, y_: batch_y, keep_prob: 0.5})
            
        test_accuracy = accuracy.eval(feed_dict={X: mnist.test.images, y_: mnist.test.labels, keep_prob:1.0})
        
        print('Test accuracy {}'.format(test_accuracy))
